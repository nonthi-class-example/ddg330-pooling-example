using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Box _boxPrefab;
    [SerializeField] private int _spawnAmount = 100;
    [SerializeField] private float _spawnInterval = 0.2f;
    [SerializeField] private float _spawnRadius = 10f;

    private void Start()
    {
        StartCoroutine(SpawnCoroutine());
    }

    private IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            yield return _spawnInterval;

            for (int i = 0; i < _spawnAmount; i++)
            {
                Box spawnedBox = Instantiate(_boxPrefab);
                spawnedBox.transform.position = transform.position + Random.insideUnitSphere * _spawnRadius;
                spawnedBox.Init(KillBox);
            }
        }
    }

    private void KillBox(Box box)
    {
        Destroy(box.gameObject);
    }
}
