using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    private Renderer _renderer;
    private Action<Box> _killAction;
    private Rigidbody _rb;

    private void Awake()
    {
        TryGetComponent(out _renderer);
        TryGetComponent(out _rb);
    }

    private void Start()
    {
        _renderer.material.color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f);
    }

    public void Init(Action<Box> killAction)
    {
        _killAction = killAction;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            _killAction?.Invoke(this);
        }
    }

    public void ResetData()
    {
        _rb.velocity = Vector3.zero;
        _rb.angularVelocity = Vector3.zero;
        transform.rotation = Quaternion.identity;
    }
}
